var app;
(function (app) {
    var casualUrl = "/api/Stats/{{platform}}/{{id}}";
    var rankedUrl = "/api/Stats/ranked/{{platform}}/{{id}}";
    var Platform = (function () {
        function Platform(name, code) {
            this.name = name;
            this.code = code;
        }
        return Platform;
    }());
    app.Platform = Platform;
    var Gamer = (function () {
        function Gamer(gamertag, platform) {
            this.gamertag = gamertag;
            this.platform = platform;
            this.loading = true;
            this.stats = {};
            this.rankedStats = {};
            this.generalStats = {};
            var $http = angular.injector(['ng', 'app']).get('$http');
            var $q = angular.injector(['ng', 'app']).get('$q');
            var me = this;
            var deferred = $q.defer();
            this.loaded = deferred.promise;
            $http.get(casualUrl.replace('{{platform}}', platform.code).replace('{{id}}', gamertag)).then(function (result) {
                me.stats = {};
                me.generalStats = {};
                angular.forEach($(result.data).find('.content'), function (i) { me.stats[$(i).find('.title').text().trim()] = isNaN(parseFloat($(i).find('.value').text())) ? $(i).find('.value').text().trim() : parseFloat($(i).find('.value').text().split(',').join('')); });
                angular.forEach($(result.data).find('.general-stats').find('tr'), function (row) {
                    var cells = $(row).find('td');
                    if (cells.length == 2) {
                        me.generalStats[cells.first().text()] = parseFloat(cells.last().text().split(',').join(''));
                    }
                });
                $http.get(rankedUrl.replace('{{platform}}', platform.code).replace('{{id}}', gamertag)).then(function (result) {
                    angular.forEach($(result.data).find('.content'), function (i) { me.rankedStats[$(i).find('.title').text().trim()] = isNaN(parseFloat($(i).find('.value').text())) ? $(i).find('.value').text().trim() : parseFloat($(i).find('.value').text().split(',').join('')); });
                    me.success = Object.keys(me.stats).length != 0;
                    me.loading = false;
                    deferred.resolve();
                }, function () {
                    me.loading = false;
                    me.success = false;
                    deferred.reject();
                });
            }, function () {
                me.loading = false;
                me.success = false;
                deferred.reject();
            });
        }
        return Gamer;
    }());
    app.Gamer = Gamer;
    var appController = (function () {
        function appController($scope) {
            this.$scope = $scope;
            this.loginInfo = "THIS IS SET CUZ WE USE R6 SITE NOW";
            this.platforms = [
                new Platform('Xbox One', 'xone'),
                new Platform('Playstation 4', 'ps4'),
                new Platform('UPlay', 'uplay')
            ];
            this.knownTags = [
                new Gamer("lts fatal", this.platforms[0]),
                new Gamer("brad2495", this.platforms[0]),
                new Gamer("scaramanga42", this.platforms[0]),
                new Gamer("diabetez", this.platforms[0]),
                new Gamer("jstock", this.platforms[0]),
                new Gamer("taboobat", this.platforms[0]),
                new Gamer("trs ckb", this.platforms[0]),
                new Gamer("m0otang", this.platforms[0]),
                new Gamer("them00gle", this.platforms[0]),
                new Gamer("joeker66", this.platforms[0]),
                new Gamer("crackerjackshot", this.platforms[0]),
                new Gamer("captain polzin", this.platforms[0])
            ];
            this.selectedPlatform = this.platforms[0];
            this.selectedMode = 'total';
            this.modes = ['total', 'hourly'];
            this.gamers = new Array();
        }
        appController.prototype.persistLogin = function () {
        };
        appController.prototype.addGamertag = function () {
            this.addSpecific(this.gamerTag, this.selectedPlatform);
        };
        appController.prototype.addSpecific = function (name, platform) {
            var _this = this;
            var gamer = new Gamer(name, platform);
            this.gamers.push(gamer);
            this.gamerTag = '';
            gamer.loaded.finally(function () {
                _this.$scope.$apply();
            });
        };
        appController.prototype.removeGamertag = function (tag) {
            var idx = this.gamers.indexOf(tag);
            this.gamers.splice(idx, 1);
        };
        appController.$inject = [
            '$scope'
        ];
        return appController;
    }());
    app.appController = appController;
    angular
        .module('app')
        .controller('appController', appController);
})(app || (app = {}));
//# sourceMappingURL=app-controller.js.map