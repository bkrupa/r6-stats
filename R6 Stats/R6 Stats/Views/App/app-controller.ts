﻿module app {
    const casualUrl: string = "/api/Stats/{{platform}}/{{id}}";
    const rankedUrl: string = "/api/Stats/ranked/{{platform}}/{{id}}";

    export class Platform {
        constructor(public name: string, public code: string) { }
    }

    export class Gamer {

        public loading: boolean = true;
        public stats: any = {};
        public rankedStats: any = {};
        public generalStats: any = {};
        public loaded: ng.IPromise<any>;
        public success: boolean;


        constructor(public gamertag: string, public platform: Platform) {
            var $http: ng.IHttpService = angular.injector(['ng', 'app']).get('$http');
            var $q: ng.IQService = angular.injector(['ng', 'app']).get('$q');
            var me = this;

            var deferred: ng.IDeferred<any> = $q.defer();
            this.loaded = deferred.promise;

            $http.get(casualUrl.replace('{{platform}}', platform.code).replace('{{id}}', gamertag)).then((result) => {
                me.stats = {};
                me.generalStats = {};
                angular.forEach($(result.data).find('.content'), (i) => { me.stats[$(i).find('.title').text().trim()] = isNaN(parseFloat($(i).find('.value').text())) ? $(i).find('.value').text().trim() : parseFloat($(i).find('.value').text().split(',').join('')); });

                angular.forEach($(result.data).find('.general-stats').find('tr'), (row) => {
                    var cells = $(row).find('td');
                    if (cells.length == 2) {
                        me.generalStats[cells.first().text()] = parseFloat(cells.last().text().split(',').join(''));
                    }
                });

                $http.get(rankedUrl.replace('{{platform}}', platform.code).replace('{{id}}', gamertag)).then((result) => {

                    angular.forEach($(result.data).find('.content'), (i) => { me.rankedStats[$(i).find('.title').text().trim()] = isNaN(parseFloat($(i).find('.value').text())) ? $(i).find('.value').text().trim() : parseFloat($(i).find('.value').text().split(',').join('')); });

                    me.success = Object.keys(me.stats).length != 0;
                    me.loading = false;
                    deferred.resolve();
                }, () => {
                    me.loading = false;
                    me.success = false;
                    deferred.reject();
                });
            }, () => {
                me.loading = false;
                me.success = false;
                deferred.reject();
            });
        }
    }

    export class appController {
        'use strict';

        protected username: string;
        protected password: string;
        protected loginInfo: string = "THIS IS SET CUZ WE USE R6 SITE NOW";

        public platforms: Array<Platform> = [
            new Platform('Xbox One', 'xone'),
            new Platform('Playstation 4', 'ps4'),
            new Platform('UPlay', 'uplay')
        ];

        public knownTags: Array<Gamer> = [
            new Gamer("lts fatal", this.platforms[0]),
            new Gamer("brad2495", this.platforms[0]),
            new Gamer("scaramanga42", this.platforms[0]),
            new Gamer("diabetez", this.platforms[0]),
            new Gamer("jstock", this.platforms[0]),
            new Gamer("taboobat", this.platforms[0]),
            new Gamer("trs ckb", this.platforms[0]),
            new Gamer("m0otang", this.platforms[0]),
            new Gamer("them00gle", this.platforms[0]),
            new Gamer("joeker66", this.platforms[0]),
            new Gamer("crackerjackshot", this.platforms[0]),
            new Gamer("captain polzin", this.platforms[0])
        ];

        public gamerTag: string;
        public selectedPlatform: Platform = this.platforms[0];

        public selectedMode: string = 'total';
        public modes: Array<string> = ['total', 'hourly'];

        protected gamers: Array<Gamer>;

        static $inject = [
            '$scope'
        ];

        constructor(
            public $scope: ng.IScope
        ) {
            this.gamers = new Array<Gamer>();
        }

        persistLogin(): void {


        }

        addGamertag(): void {
            this.addSpecific(this.gamerTag, this.selectedPlatform);
        }

        addSpecific(name: string, platform: Platform): void {
            var gamer: Gamer = new Gamer(name, platform);
            this.gamers.push(gamer);
            this.gamerTag = '';

            gamer.loaded.finally(() => {
                this.$scope.$apply();
            });
        }

        removeGamertag(tag: Gamer): void {
            var idx = this.gamers.indexOf(tag);
            this.gamers.splice(idx, 1);
        }

    }

    angular
        .module('app')
        .controller('appController', appController);
}