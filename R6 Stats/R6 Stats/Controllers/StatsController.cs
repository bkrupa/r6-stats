﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace R6_Stats.Controllers
{
    [RoutePrefix("api/Stats")]
    public class StatsController : ApiController
    {
        [Route("{platform}/{username}")]
        public async Task<string> GetStats(string platform, string username)
        {
            WebRequest request = WebRequest.Create("https://r6stats.com/stats/{{platform}}/{{id}}/casual".Replace("{{platform}}", platform).Replace("{{id}}", username));
            WebResponse response = await request.GetResponseAsync();

            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                return await reader.ReadToEndAsync();
        }

        [Route("ranked/{platform}/{username}")]
        public async Task<string> GetRankedStats(string platform, string username)
        {
            WebRequest request = WebRequest.Create("https://r6stats.com/stats/{{platform}}/{{id}}/ranked".Replace("{{platform}}", platform).Replace("{{id}}", username));
            WebResponse response = await request.GetResponseAsync();

            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                return await reader.ReadToEndAsync();
        }

    }
}
